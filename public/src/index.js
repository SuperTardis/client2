import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom'
import { Provider } from 'react-redux'
import Routes from './routes'
import configureStore from './store/configureStore'
import styles from '../styles/style.css'

export const store = configureStore();

ReactDOM.render((
	<Provider store={store}>
	<BrowserRouter>
		<Routes/>
	</BrowserRouter>
	</Provider>

), document.getElementById('container'));
