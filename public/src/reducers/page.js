import * as Actions from '../actions/actions'
import { reducer as formReducer } from 'redux-form'

const initialState = {
	id: -1,
	films: [],
	isShowing: false,
	filteredFilms: []
};

export default function page(state = initialState, action) {

	console.log(action.type);
	switch (action.type) {
		case Actions.GET_FILMS:
			return {...state, response: action.payload.response};
		case Actions.GET_FILMS_SUCCESS:
			return {...state, id :1 , films: action.payload, filteredFilms: action.payload};
		case Actions.ADD_FILM:
			return {...state, id :1 , response: action.payload.response};
		case Actions.ADD_FILM_SUCCESS:
			return {...state, id :1 , response: action.payload};
		case Actions.DELETE_FILM:
			return {...state, id :1, response: action.payload};
		case Actions.DELETE_FILM_SUCCESS:
			return {...state, film:action.payload};
		case Actions.SHOW_MODAL:
			return {...state, isShowing: true, film:action.payload};
		case Actions.HIDE_MODAL:
			return {...state, isShowing: false};
		case Actions.SET_FIlTER_FILMS:
			return {...state, filteredFilms: action.payload};
		default:
			return state;
	}
}