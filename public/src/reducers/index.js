import { combineReducers } from 'redux'
import page from './page'
import { reducer as FormReducer } from 'redux-form'

export default combineReducers({
	page,
	form: FormReducer,
})
