import { createStore, applyMiddleware } from 'redux'
import rootReducer from '../reducers'
import thunk from 'redux-thunk'
import promise from 'redux-promise'

export default function configureStore(initialState) {
	const store = createStore(rootReducer, initialState,  applyMiddleware(thunk), applyMiddleware(promise));
	if (module.hot) {
		module.hot.accept('../reducers', () => {
			const nextRootReducer = require('../reducers');
			store.replaceReducer(nextRootReducer)
		})
	}
	return store
}