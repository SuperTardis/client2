import axios from 'axios';

export const GET_FILMS = 'GET_FILMS';
export const GET_FILMS_SUCCESS = 'GET_FILMS_SUCCESS';

export const ADD_FILM = 'ADD_FILM';
export const ADD_FILM_SUCCESS = 'ADD_FILM_SUCCESS';

export const DELETE_FILM = 'DELETE_FILM';
export const DELETE_FILM_SUCCESS = 'DELETE_FILM_SUCCESS';

export const UPLOAD_FILM = 'UPLOAD_FILM';
export const UPLOAD_FILM_SUCCESS = 'UPLOAD_FILM_SUCCESS';

export const SHOW_DETAILS = 'SHOW_DETAILS';
export const CLOSE_DETAILS = 'CLOSE_DETAILS';

export const ROOT_URL = 'http://127.0.0.1:8089';

export const SHOW_MODAL = 'SHOW_MODAL';
export const HIDE_MODAL = 'HIDE_MODAL';
export const SET_NAME = 'SET_NAME';

export const SET_FIlTER_FILMS = 'SET_FIlTER_FILMS';

export function showModal(film) {
	return {
		type: SHOW_MODAL,
		payload: film
	}
}

export function setFilterFilms(films) {
	return {
		type: SET_FIlTER_FILMS,
		payload: films
	}
}

export function hideModal() {
	return {
		type: HIDE_MODAL
	}
}

export const showDetails = (film) => {
	return {
		type: SHOW_DETAILS,
		payload: film
	}
};

export const closeDetails = () => {
	return {
		type: CLOSE_DETAILS,
		payload: undefined
	}
};

export const getFilms = (data) => {
	const request = axios.get(ROOT_URL + '/films', {
		params: {
			search: data
		}
	});
	return {
		type: GET_FILMS,
		payload: request
	}
};

export const deleteFilm = (id)=> {
	const request = axios({
		method: 'delete',
		url: ROOT_URL + '/films/' + id ,
	});
	return {
		type: DELETE_FILM,
		payload: request
	}
};

export const deleteFilmSuccess = (id)=> {
	return {
		type: DELETE_FILM_SUCCESS,
		payload: id
	}
};

export const getFilmsSuccess = (films) => {
	return {
		type: GET_FILMS_SUCCESS,
		payload: films
	}
};

export const addFilm = (film) => {
	const request = axios({
		method: 'post',
		data: film,
		url: ROOT_URL + '/films',
		headers: {
			'Accept': 'application/json'
		}
	});

	return {
		type: ADD_FILM,
		payload: request
	}
};

export const addFilmSuccess = () => {
	return {
		type: ADD_FILM_SUCCESS,
		payload: undefined
	}
};

export const uploadFile = (files) => {
	const formData = new FormData();
	formData.append('file', files[0]);
	const request = axios({
		method: 'post',
		data: formData,
		url: ROOT_URL + '/fileupload',
	});

	return {
		type: UPLOAD_FILM,
		payload: request
	}
};