import React, {Component} from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as pageActions from '../actions/actions'
import Modal from './Modal.jsx';

export class Filter extends Component{
	filterList = (event) =>{
		const { page, pageActions} = this.props;

		pageActions.setFilterFilms(page.films.filter((film) => {
			let stars = film.stars;
			let searcher = (str) => {
				return (str.toLowerCase().search(event.target.value.toLowerCase()) !== -1);
			};
			return stars.some((star)=> {
				let str = star.firstName + " " + star.lastName;
				return searcher(str);
			}) || searcher(film.title);
		}));
	};

	sortList = (event) =>{
		const { page, pageActions} = this.props;

		pageActions.setFilterFilms(page.films.sort((first, second)=>{
			return first.title.localeCompare(second.title);
		}));
	};

	componentWillMount() {
		this.props.pageActions.getFilms().payload.then((response) => {
			if (!response.error && response.status === 200)
				this.props.pageActions.getFilmsSuccess(response.data.films);
		});
	}

	render (){
		return (
			<div>
				<form>
					<fieldset>
						<input type="text" className="search" placeholder="Search" onChange={this.filterList}/>
					</fieldset>
				</form>
				<button id="sort" onClick={this.sortList}>Sort</button>
				<Home page={this.props.page} pageActions ={this.props.pageActions} />
			</div>
		);
	}
}

export class Home extends Component
{
	removeFilm = (even,id) => {
		even.stopPropagation();
		this.props.pageActions.deleteFilm(id).payload.then((response) => {
			if(response.status === 200)
			{
				this.props.pageActions.setFilterFilms(this.props.page.filteredFilms.filter((film) =>{
					return film._id !== id;
				}))
			}
			this.props.pageActions.deleteButtonUp();
		});
	};

	render() {
		const {pageActions, page} = this.props;
		const rmFilm = this.removeFilm;
		return (
			<div>
				<ul id="myUl">
					{this.props.page.filteredFilms.map(function(film){
						return <div>
							<li  key={film.id} onClick={() =>{if (!page.isRmPress)pageActions.showModal(film)}}>
								{film.title}
								<button id="remove_star" type="button" title="Remove Stars" onClick={event => rmFilm(event,film._id)}>&#10006;</button>
							</li>

						</div>
 					})}
				</ul>
				<Modal onCancel={pageActions.hideModal}/>
			</div>
		)
	}
}

function mapStateToProps (state) {
	return {
		page: state.page
	}
}

function mapDispatchToProps(dispatch) {
	return {
		pageActions: bindActionCreators(pageActions, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Filter);