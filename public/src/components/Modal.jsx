import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as pageActions from '../actions/actions'
import { bindActionCreators } from 'redux'

class Modal extends Component {
	render() {
		const { page, onCancel, film} = this.props;
		return (
			<div className="confirm-modal">
				{ page.isShowing &&
				<div>
					<ul>
						<div className="modal-backdrop"/>
						<div className="confirm-modal-content">
							<li className="li_1">
								<p>Title: {page.film.title}</p>
							</li>
							<li className="li_1">
								<p>Release Year: {page.film.release}</p>
							</li>
							<li className="li_1">
								<p>Format: {page.film.format.kind}</p>
							</li>
							<li className="li_2">
								Stars: <ul>
									{this.props.page.film.stars.map((star) =>{
										return <li>{star.firstName} {star.lastName} </li>;
									})}
								</ul>
							</li>
						<button className="btn" onClick={() => {onCancel(); console.log(this.props)}}>Cancel</button>
						</div>
					</ul>
				</div>
				}
			</div>
		)
	}
}

function mapStateToProps (state) {
	return {
		page: state.page
	}
}

function mapDispatchToProps(dispatch) {
	return {
		pageActions: bindActionCreators(pageActions, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Modal)