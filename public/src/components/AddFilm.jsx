import React, {Component, PropTypes} from 'react'
import { render } from 'react-dom'
import { Router, Route } from 'react-router'
import { Field, FieldArray, reduxForm } from 'redux-form';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as pageActions from '../actions/actions'
import {reset} from 'redux-form';
import { FileUpload } from 'redux-file-upload'
import Dropzone from 'react-dropzone'
import validate from './validate';

const afterSubmit = (result, dispatch) =>
	dispatch(reset('addFilms'));

const submit = (formData, dispatch) => {
	pageActions.addFilm(formData).payload.then((response) => {
		console.log(response.status);
		if (response.status === 200)
		{
			pageActions.addFilmSuccess();
			dispatch(reset('addFilms'));
		}
		else
			console.log("Error");
	});
};

const onDrop = (files) =>{
	pageActions.uploadFile(files);
};

const renderField = ({ input, label, type, meta: { touched, error } }) => (
	<div>
			<input {...input} type={type} placeholder={label} />
			{touched && error && <span>{error}</span>}
	</div>
);

export const renderSelect = ({ input, label, type, meta: { touched, error }, children }) => (
	<div>
		<select {...input}>
			{children}
		</select>
		{touched && error && <div className="error">{error}</div>}
	</div>
);

const renderStars = ({ fields, meta: { touched, error, submitFailed } }) => (
	<ul>
		<li id="add_star">
			<button type="button" onClick={() => fields.push({})}>Add Star</button>
			{(touched || submitFailed) && error && <span>{error}</span>}
		</li>
		{fields.map((star, index) => (
			<li key={index} id="remove">
				<h4>Star #{index + 1}</h4>
				<button
					type="button"
					title="Remove Stars"
					onClick={() => fields.remove(index)}
				>&#10006;</button>
				<Field
					name={`${star}.firstName`}
					type="text"
					component={renderField}
					label="First Name"
				/>
				<Field
					name={`${star}.lastName`}
					type="text"
					component={renderField}
					label="Last Name"
				/>
			</li>
		))}
	</ul>
);

const AddFilms = props => {
	const { handleSubmit, pristine, reset, submitting } = props;
	const overlayStyle = {
		padding: '2.5em 0',
		background: '#F9F9F9',
		textAlign: 'center',
		margin: '5% 5%',
		color: '#1f1f2e',
		boxShadow: '0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24)'
	};
	const active = {
		padding: '2.5em 0',
		background: '#1f1f2e',
		textAlign: 'center',
		margin: '5% 5%',
		color: 'white',
		boxShadow: '0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24)'
	};
	return (
		<div>
		<form onSubmit={handleSubmit(submit)} id="form_id">
			<Field
				name="title"
				type="text"
				component={renderField}
				label="Title"
			/>
			<Field
				name="release"
				type="number"
				component={renderField}
				label="Release Year"
			/>
			<Field name="format" component = {renderSelect}>
				<option value="" name="" disabled>Select Format</option>
				<option value="Blu-Ray" name="Blu-Ray">Blu-Ray</option>
				<option value="VHS" name="VHS">VHS</option>
				<option value="DVD" name="DVD">DVD</option>
			</Field>
			<FieldArray name="stars" component={renderStars} />
			<div id="but_div">
				<button type="submit" disabled={submitting}>Submit</button>
				<button type="button" disabled={pristine || submitting} onClick={reset}>
					Clear Values
				</button>
			</div>
		</form>
			<Dropzone style={overlayStyle} activeStyle={active} onDrop={onDrop} multiple>
				<div id="drop">Try dropping some files here, or click to select files to upload.</div>
			</Dropzone>
		</div>
	);
};


export default reduxForm({
	form: 'addFilms', // a unique identifier for this form
	validate,
})(AddFilms);