const validate = values => {
	const errors = {};
	if (!values.title) {
		errors.title = 'Required';
	}
	if (!values.release) {
		errors.release = 'Required';
	}
	if (!values.format) {
		errors.format = 'Required';
	}
	if (!values.stars || !values.stars.length) {
		errors.stars = { _error: 'At least one stars must be entered' };
	} else {
		const starsArrayErrors = [];
		values.stars.forEach((stars, starsIndex) => {
			const starsErrors = {};
			if (!stars || !stars.firstName) {
				starsErrors.firstName = 'Required';
				starsArrayErrors[starsIndex] = starsErrors;
			}
			if (!stars || !stars.lastName) {
				starsErrors.lastName = 'Required';
				starsArrayErrors[starsIndex] = starsErrors;
			}
		});
		if (starsArrayErrors.length) {
			errors.stars = starsArrayErrors;
		}
	}
	return errors;
};

export default validate;
