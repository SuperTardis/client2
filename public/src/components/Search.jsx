import React, {Component, PropTypes} from 'react'
import { render } from 'react-dom'
import { Router, Route } from 'react-router'
import { Field, FieldArray, reduxForm } from 'redux-form';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as pageActions from '../actions/actions'
import {reset} from 'redux-form';
import { FileUpload } from 'redux-file-upload'
import Dropzone from 'react-dropzone'
import validate from './validate';

const submit = (formData, dispatch) => {
	pageActions.getFilms(formData.search).payload.then((response) => {
		(!response.error && response.status === 200)
		{
			console.log(response.data.films);
			pageActions.getFilmsSuccess(response.data.films);
		}
	});
};

const Search = props => {
	const { handleSubmit, submitting } = props;
	return (
		<div>
			<form onSubmit={handleSubmit(submit)}>
				<Field
					name="search"
					type="text"
					component="input"
					label="Title"
				/>
				<div>
					<button type="submit" disabled={submitting}>Search</button>
				</div>
			</form>
		</div>
	);
};


export default reduxForm({
	form: 'search', // a unique identifier for this form
	validate,
})(Search);